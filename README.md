# Mazes!
**This is a maze building and solving repository for Jason Kolbly's 20% time.**

If you like it, let me know!

And if you wanna use my code, can you please ask first?

I'll say yes, but please tell me first.

# Requirements
#### PyGame:
Only tested with PyGame version 1.9.3, but it may work with others.

# Usage
Open the maze_quit.py file and use the interactive GUI to select maze methods. Press the "?" button for a help message printed to the console.

# Internal Methods
**Here's how it works on the inside and the methods I used.**

### Solving
To solve a maze, it uses a breadth first search. Essentially, the starting tile is given a distance value of 0. Each adjacent non-wall tile is given a distance of 1. The non-visited, non-wall neighbors of those tiles are given a distance of 2. This is repeated until it reaches the end, at which point it retraces its steps, following the lower distance values until it reaches the beginning.

### Generation
There are 3 methods, but they are all basically the same. Essentially, choose a starting tile and add it to the stack. While there are tiles in the stack, choose a tile from the stack and carve a passage to one of its unvisited neighbors, and add its neighbors to the stack. Remove the initial tile from the stack.

The method comes into play by deciding how you choose to select tiles from the stack. The methods are as follows:
#### Prim's
Randomly select a tile from the stack.
#### Snake
Choose the most recently added tile from the stack.
#### Straight (Boring)
Choose the oldest tile in the stack.