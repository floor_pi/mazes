import Maze
import tkinter as tk
from tkinter import messagebox
import os

maze = None
screen = None

methods_text = """Solving:
To solve a maze, it uses a breadth first search. Essentially, the starting tile is given a distance value of 0. Each adjacent non-wall tile is given a distance of 1. The non-visited, non-wall neighbors of those tiles are given a distance of 2. This is repeated until it reaches the end, at which point it retraces its steps, following the lower distance values until it reaches the beginning.

Generation:
There are 3 methods, but they are all basically the same. Essentially, choose a starting tile and add it to the stack. While there are tiles in the stack, choose a tile from the stack and carve a passage to one of its unvisited neighbors, and add its neighbors to the stack. Remove the initial tile from the stack. The method comes into play by deciding how you choose to select tiles from the stack. The methods are as follows:

Prim's: Randomly select a tile from the stack.

Snake: Choose the most recently added tile from the stack.

Straight (Boring): Choose the oldest tile in the stack."""


help_msg = """New Maze: Create a new maze box with the chosen width and height. This will let you make your own mazes

Slow Down: Check if you want to see the path unfurl when solving or see the maze building itself. Be careful with large mazes, it may take a while.

Select Start and End: Click to select the startpoint on a user-made maze. After it is selected, it will prompt you to also select the end point.

Width/Height: Set the size of the maze generated with Generate or New Maze.

Prim: Prim's algorithm. Selects tiles from the stack randomly.

Snake: Generates in a snakey pattern. Selects the most recently added tile from the stack.

Boring: Pretty stupid mazes. Selects the first added tile in the stack.

Generate: Randomly create a maze. Will make a new maze box if there isn't one already.

Solve: Solve the maze. Does nothing if there is not maze box.

Show Numbers: Use this too visualize the way the solver works. Be careful with big mazes, it takes a really long time.

Save Name: The name that the maze will be saved under. Don't include an extension, it is automatically a .txt file of the proper format.

Save: Save the current maze in the "mazes" folder under the chosen name, as long as the maze is completely built.

File Name: The file name to load a maze from. It only takes .txt files of the proper format.

Load: Load the chosen file from the "mazes" folder. It will open a new maze for you.

Close Maze: Close the current maze box, but keep the program open.

QUIT: Close the program.

?: Display this message."""

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        global selection_type, should_wait, educational_bool

        # New Maze button
        self.new_maze = tk.Button(self, text="New Maze", command=new_maze, fg="blue")
        self.new_maze.grid(row=0,column=0,columnspan=1,padx=10,pady=10)

        # Boolean for wait time
        should_wait = tk.BooleanVar(self, False)

        # Wait time checkbox
        self.wait = tk.Checkbutton(self, text="Slow Down", variable=should_wait, onvalue=True, offvalue=False)
        self.wait.grid(row=0,column=1,columnspan=1,padx=10,pady=10)

        # Select Start/End button
        self.select_start_and_end = tk.Button(self, text="Select Start and End", command=select_start, fg="blue")
        self.select_start_and_end.grid(row=0,column=2,columnspan=1,padx=10,pady=10)
        
        # Width label and entry
        tk.Label(self, text="Width:").grid(row=1,column=0,columnspan=2)
        self.width = tk.Entry(self)
        self.width.grid(row=1,column=1,columnspan=5,padx=10)
        self.width.insert(tk.END, "50")

        # Height label and entry
        tk.Label(self, text="Height:").grid(row=2,column=0,columnspan=2)
        self.height = tk.Entry(self)
        self.height.grid(row=2,column=1,columnspan=5,padx=10)
        self.height.insert(tk.END, "50")

        # An integer for selection type
        selection_type = tk.StringVar(self, "prim")

        # Radio buttons
        prim_button = tk.Radiobutton(self, text="Prim", variable=selection_type, value="prim")
        prim_button.grid(row=3,column=0,padx=10,pady=10)

        snake_button = tk.Radiobutton(self, text="Snake", variable=selection_type, value="snake")
        snake_button.grid(row=3,column=1,padx=10,pady=10)

        boring_button = tk.Radiobutton(self, text="Boring", variable=selection_type, value="straight")
        boring_button.grid(row=3,column=2,padx=10,pady=10)

        # Generate button
        self.generate = tk.Button(self, text="Generate", command=generate, fg="blue")
        self.generate.grid(column=0,columnspan=1,padx=10,pady=10)

        # Solve button
        self.solve = tk.Button(self, text="Solve", command=solve, fg="blue")
        self.solve.grid(row=4,column=1,columnspan=1,padx=10,pady=10)

        # Show Numbers checkbox
        educational_bool = tk.BooleanVar(self, False)

        # Wait time checkbox
        self.educational = tk.Checkbutton(self, text="Show Numbers", variable=educational_bool, onvalue=True, offvalue=False)
        self.educational.grid(row=4,column=2,columnspan=1,padx=10,pady=10)

        # Save Name entry box
        tk.Label(self, text="Save Name:").grid(row=5,column=0,columnspan=1)
        self.save_name = tk.Entry(self)
        self.save_name.grid(row=5,column=1,columnspan=1,padx=10,pady=10)

        # Save button
        self.save = tk.Button(self, text="Save", command=save, fg="blue")
        self.save.grid(row=5,column=2,columnspan=1,padx=10,pady=10)

        # File Name entry box
        tk.Label(self, text="File Name:").grid(row=6,column=0,columnspan=1)
        self.file_name = tk.Entry(self)
        self.file_name.grid(row=6,column=1,columnspan=1,padx=10,pady=10)

        # Load button
        self.load = tk.Button(self, text="Load", command=load, fg="blue")
        self.load.grid(row=6,column=2,columnspan=1,padx=10,pady=10)

        # Close Maze button
        self.close_maze = tk.Button(self, text="Close Maze", command=close_maze, fg="red")
        self.close_maze.grid(row=7,column=0,columnspan=1,padx=10,pady=10)

        # QUIT button
        self.quit = tk.Button(self, text="QUIT", command=killall, fg="red")
        self.quit.grid(row=7,column=1,columnspan=1,padx=10,pady=10)

        # Methods button
        self.methods = tk.Button(self, text="Methods", command=display_methods, fg="black")
        self.methods.grid(row=7,column=2,columnspan=1,padx=10,pady=10)

        # ? button
        self.help = tk.Button(self, text="?", command=display_help, fg="black")
        self.help.grid(row=7,column=3,columnspan=1,padx=10,pady=10)

def killall():
    running = False
    Maze.kill()
    root.destroy()
    quit()

def close_maze():
    global maze, screen
    maze = None
    screen = None
    Maze.kill_display()
    app.new_maze.config(fg="blue")

def select_start():
    if maze != None:
        #print("Select the start position.")
        maze.click_phase = 1

def load():
    global maze, screen
    if app.file_name.get() != "":
        if not os.path.exists(os.path.join(os.path.dirname(__file__), "mazes", app.file_name.get() + ".txt")):
            #print("File does not exist!")
            Maze.throw_error("Invalid Path","File does not exist!")
            return False
        if maze != None:
            close_maze()
        maze = Maze.maze((10,10), file_name=app.file_name.get())
        screen = Maze.init_display(maze.get_size(), "Maze!")
        maze.set_screen(screen)

def save():
    if maze != None and app.save_name.get() != "" and maze.built:
        maze.save(app.save_name.get())

def display_help():
    #print(help_msg)
    messagebox.showinfo("Help",help_msg)

def display_methods():
    #print(methods_text)
    messagebox.showinfo("Methods",methods_text)

def solve():
    if maze != None and maze.built:
        maze.solve(should_wait.get(), educational_bool.get())

def generate():
    global maze
    if maze == None:
        if not new_maze():
            return False
    try:
        if len(maze.maze) != int(app.height.get()) or len(maze.maze[0]) != int(app.width.get()):
            close_maze()
            new_maze()
    except ValueError:
        #print("Use a number!")
        Maze.throw_error("ValueError","Please use a number.")
        return False
    maze.built = False
    maze.generate(selection_type.get(), should_wait.get())

def new_maze():
    global maze, screen
    size = (-1,-1)
    try:
        size = (int(app.width.get()),int(app.height.get()))
    except ValueError:
        #print("Use a number!")
        Maze.throw_error("ValueError","Please use a number.")
        return False
    if size[0] > 200 or size[1] > 200 or size[0] < 5 or size[1] < 5:
        #print("Please make the width and height from 5 to 200.")
        Maze.throw_error("Invalid Size","Please make the width and height from 5 to 200.")
        return False
    close_maze()
    maze = Maze.maze(size)
    screen = Maze.init_display(maze.get_size(), "Maze!")
    maze.set_screen(screen)
    return True

def error_boxes():
    for error in Maze.errors:
        messagebox.showerror(error["name"], error["message"])

root = tk.Tk()
app = Application(master=root)

running = True

while running:
    Maze.clear_errors()
    if maze != None and screen != None:
        Maze.handle_events(screen)
        maze.draw()
        maze.click_handler()
    app.update_idletasks()
    app.update()
    error_boxes()
