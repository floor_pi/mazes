import pygame
import os
from random import randint, choice
from math import sqrt

pygame.init()

# Words that mean yes
yes_synonyms = ["y", "yes", "sure", "okay", "fine", "affirmative", "all right", "very well", "of course", "by all means", "certainly", "absolutely", "indeed", "right", "agreed", "roger", "ok", "yeah", "yep", "yup", "okey-dokey", "yea", "aye"]

# The colors used in the program
white = (255,255,255)
black = (0,0,0)
purple = (255,0,255)
blue = (0,0,255)
green = (0,255,0)
gray = (100,100,100)

# The strings used in the board format
blank_string = "#"
wall_string = "."
start_string = "&"
end_string = "@"
dead_end_string = "*"

def throw_error(error_name, error_message):
    global errors
    errors.append({"name":error_name,"message":error_message})

def clear_errors():
    global errors
    errors = []

# Get user input for the size of a maze. It takes a boolean argument for whether you want a square maze
def get_maze_size(square = False):
    maze_size = [0,0]
    if square:
        while not (maze_size[0] >= 5):
            maze_size[0] = input("How big do you want it to be? (It'll be a square) ")
            try:
                maze_size[0] = int(maze_size[0])
                if not maze_size[0] >= 5:
                    print("Please make it at least 5!")
                    throw_error("Too Small","Please make it at least 5!")
                    pass
            except ValueError:
                print("Please make it an integer.")
                throw_error("ValueError","Please make it an integer.")
                maze_size[0] = 0
        maze_size[1] = maze_size[0]
    else:
        while not (maze_size[0] >= 5):
            maze_size[0] = input("How wide do you want it to be? ")
            try:
                maze_size[0] = int(maze_size[0])
                if not maze_size[0] >= 5:
                    print("Please make it at least 5!")
                    throw_error("Too Small","Please make it at least 5!")
                    pass
            except ValueError:
                print("Please make it an integer.")
                throw_error("ValueError","Please make it an integer.")
                maze_size[0] = 0
        while not (maze_size[1] >= 5):
            maze_size[1] = input("How tall do you want it to be? ")
            try:
                maze_size[1] = int(maze_size[1])
                if not maze_size[1] >= 5:
                    print("Please make it at least 5!")
                    throw_error("Too Small","Please make it at least 5!")
                    pass
            except ValueError:
                print("Please make it an integer.")
                throw_error("ValueError","Please make it an integer.")
                maze_size[1] = 0
    return maze_size

# Initiate pygame's display, then return the screen
def init_display(size, caption="Maze Fun!"):
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption(caption)
    return screen

def kill_display():
    pygame.display.quit()

def kill():
    pygame.quit()
    #quit()

def handle_events(screen):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            kill()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                kill()
            elif event.key == pygame.K_s:
                for maze in mazes:
                    if maze.click_phase == 0:
                        maze.save(screen)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            for maze in mazes:
                maze.click_board()

# Return the neighbors for generation
def neighbors(maze, cell):
    neighbors = []
    x = cell[0]
    y = cell[1]

    if x-2 >= 0 and maze.maze[y][x-2] == wall_string:
        neighbors.append([x-2,y])
    if x+2 <= len(maze.maze[0]) - 1 and maze.maze[y][x+2] == wall_string:
        neighbors.append([x+2,y])
    if y-2 >= 0 and maze.maze[y-2][x] == wall_string:
        neighbors.append([x,y-2])
    if y+2 <= len(maze.maze) - 1 and maze.maze[y+2][x] == wall_string:
        neighbors.append([x,y+2])
    return neighbors

# Mark neighbors for solving
def mark_neighbors(maze, start_distance, cell):
    try:
        if maze.maze[cell[1]][cell[0]-1] != wall_string and cell[0] > 0 and maze.distances[cell[1]][cell[0]-1] == -1:
            if maze.maze[cell[1]][cell[0]-1] == end_string:
                print("The end is " + str(start_distance+1) + " away from the start.")
                maze.distances[cell[1]][cell[0]-1] = start_distance+1
                del maze.marking_stack[:]
                return True
            else:
                maze.distances[cell[1]][cell[0]-1]  = start_distance + 1
                maze.marking_stack.append([cell[0]-1,cell[1],start_distance + 1])
    except IndexError:
        pass
    try:
        if maze.maze[cell[1]][cell[0]+1] != wall_string and cell[0] < len(maze.maze[cell[1]]) and maze.distances[cell[1]][cell[0]+1] == -1:
            if maze.maze[cell[1]][cell[0]+1] == end_string:
                print("The end is " + str(start_distance+1) + " away from the start.")
                maze.distances[cell[1]][cell[0]+1] = start_distance+1
                del maze.marking_stack[:]
                return True
            else:
                maze.distances[cell[1]][cell[0]+1]  = start_distance + 1
                maze.marking_stack.append([cell[0]+1,cell[1],start_distance + 1])
    except IndexError:
        pass
    try:
        if maze.maze[cell[1]-1][cell[0]] != wall_string and cell[1] > 0 and maze.distances[cell[1]-1][cell[0]] == -1:
            if maze.maze[cell[1]-1][cell[0]] == end_string:
                print("The end is " + str(start_distance+1) + " away from the start.")
                maze.distances[cell[1]-1][cell[0]] = start_distance+1
                del maze.marking_stack[:]
                return True
            else:
                maze.distances[cell[1]-1][cell[0]]  = start_distance + 1
                maze.marking_stack.append([cell[0],cell[1]-1,start_distance + 1])
    except IndexError:
        pass
    try:
        if maze.maze[cell[1]+1][cell[0]] != wall_string and cell[1] < len(maze.maze) and maze.distances[cell[1]+1][cell[0]] == -1:
            if maze.maze[cell[1]+1][cell[0]] == end_string:
                print("The end is " + str(start_distance+1) + " away from the start.")
                maze.distances[cell[1]+1][cell[0]] = start_distance+1
                del maze.marking_stack[:]
                return True
            else:
                maze.distances[cell[1]+1][cell[0]]  = start_distance + 1
                maze.marking_stack.append([cell[0],cell[1]+1,start_distance + 1])
    except IndexError:
        pass
    return True

def return_lower(maze, coords):
    try:
        if maze.distances[coords[1]][coords[0]-1] < maze.distances[coords[1]][coords[0]] and maze.distances[coords[1]][coords[0]-1] > -1:
            return (coords[0]-1, coords[1])
    except IndexError:
        pass
    try:
        if maze.distances[coords[1]][coords[0]+1] < maze.distances[coords[1]][coords[0]] and maze.distances[coords[1]][coords[0]+1] > -1:
            return (coords[0]+1, coords[1])
    except IndexError:
        pass
    try:
        if maze.distances[coords[1]-1][coords[0]] < maze.distances[coords[1]][coords[0]] and maze.distances[coords[1]-1][coords[0]] > -1:
            return (coords[0], coords[1]-1)
    except IndexError:
        pass
    try:
        if maze.distances[coords[1]+1][coords[0]] < maze.distances[coords[1]][coords[0]] and maze.distances[coords[1]+1][coords[0]] > -1:
            return (coords[0], coords[1]+1)
    except IndexError:
        pass

mazes = []

class maze():
    def __init__(self, size, screen=None, file_name=""):
        global mazes
        
        # Set the blank maze (blank = no walls)
        self.clear(size)

        self.built = False

        if file_name != "":
            self.load(file_name)

        # Set the size of each tile for this maze
        self.tile_size = int(600/len(self.maze))

        self.click_phase = 0
        self.clicking_wall = False

        self.marking_stack = []
        self.solution = []

        self.screen = screen

        mazes.append(self)

    def set_screen(self, screen):
        self.screen = screen

    # Draw the maze on a given screen
    def draw(self, numbers=False):
        self.screen.fill(white)
        for y in range(len(self.maze)):
            for x in range(len(self.maze[y])):
                if self.maze[y][x] == wall_string:
                    pygame.draw.rect(self.screen, black, [x*self.tile_size,y*self.tile_size,self.tile_size,self.tile_size])
                elif self.maze[y][x] == end_string:
                    pygame.draw.rect(self.screen, blue, [x*self.tile_size,y*self.tile_size,self.tile_size,self.tile_size])
                elif self.maze[y][x] == start_string:
                    pygame.draw.rect(self.screen, purple, [x*self.tile_size,y*self.tile_size,self.tile_size,self.tile_size])
                elif self.maze[y][x] == dead_end_string:
                    pygame.draw.rect(self.screen, gray, [y*self.tile_size,x*self.tile_size,self.tile_size,self.tile_size])

                if numbers and self.distances[y][x] > -1:
                    distance_text = font.render(str(self.distances[y][x]), False, gray)
                    text_size = distance_text.get_size()
                    self.screen.blit(distance_text, [x*self.tile_size + (self.tile_size/2) - text_size[0]/2, y*self.tile_size + (self.tile_size/2) - text_size[1]/2])
        for tile in self.solution:
            pygame.draw.rect(self.screen, green, [tile[0]*self.tile_size,tile[1]*self.tile_size,self.tile_size,self.tile_size])
        pygame.display.update()

    def get_size(self):
        size = [len(self.maze[0])*self.tile_size, len(self.maze)*self.tile_size]
        return size

    def save(self, save_name=""):
        path = os.path.dirname(__file__)

        if self.built == True:
            self.click_phase = 3
        
        if self.click_phase == 0:
            self.click_phase = 1
            print("Select the start position.")
            return False
        if self.click_phase != 3:
            return False
        if not self.built:
            if self.click_phase == 0:
                self.click_phase = 1
                print("Select the start position.")
                return False
            if self.click_phase != 3:
                return False

            self.draw()
            
            if save_name == "" or not input("Are you sure it's ready? ") in yes_synonyms:
                return False

        if save_name == "":
            if  not input("Are you sure it's ready? ") in yes_synonyms:
                for y in range(len(self.maze)):
                        for x in range(len(self.maze[y])):
                            if self.maze[y][x] in "@&":
                                self.maze[y][x] = "#"
                self.click_phase = 0
                return False


        while len(save_name) == 0:
            save_name = input("What do you want to save it as? (Don't include the extension) ")

        path = os.path.dirname(__file__)
        save_name = os.path.join(path, "mazes", save_name + ".txt")

        file = open(save_name, "w+")

        file.write("width: " + str(len(self.maze[0])) + " height: " + str(len(self.maze)) + "\n")

        temp_board = self.maze
        for row in temp_board:
            row = str(row)
            row = row.replace("[","")
            row = row.replace("]","")
            row = row.replace("'","")
            row = row.replace(" ","")
            file.write(row + "\n")

        print("File saved!")

        self.built = True

    def click_handler(self):
        if not self.built:
            mouse_coords = pygame.mouse.get_pos()
            clicked_coords = [int(mouse_coords[0]/self.tile_size),int(mouse_coords[1]/self.tile_size)]
            if pygame.mouse.get_pressed()[0] and self.click_phase == 0:
                if self.clicking_wall:
                    self.maze[clicked_coords[1]][clicked_coords[0]] = blank_string
                else:
                    self.maze[clicked_coords[1]][clicked_coords[0]] = wall_string

    def click_board(self):
        if not self.built:
            mouse_coords = pygame.mouse.get_pos()
            clicked_coords = [int(mouse_coords[0]/self.tile_size),int(mouse_coords[1]/self.tile_size)]
            if self.maze[clicked_coords[1]][clicked_coords[0]] == blank_string:
                self.clicking_wall = False
                if self.click_phase == 1:
                    self.maze[clicked_coords[1]][clicked_coords[0]] = start_string
                    self.click_phase = 2
                    print("Select the end point.")
                elif self.click_phase == 2:
                    self.maze[clicked_coords[1]][clicked_coords[0]] = end_string
                    self.click_phase = 3
                    self.built = True
                    #self.save()
            elif self.maze[clicked_coords[1]][clicked_coords[0]] == wall_string:
                self.clicking_wall = True
                if self.click_phase != 0:
                    for y in range(len(self.maze)):
                        for x in range(len(self.maze[y])):
                            if self.maze[y][x] in [end_string, start_string]:
                                self.maze[y][x] = blank_string
                    self.click_phase = 0
                    print("Saving aborted!")
            elif self.maze[clicked_coords[1]][clicked_coords[0]] == start_string:
                self.maze[clicked_coords[1]][clicked_coords[0]] = blank_string
                self.click_phase = 1
                if self.click_phase == 3:
                    for y in range(len(self.maze)):
                        for x in range(len(self.maze[y])):
                            if self.maze[y][x] == end_string:
                                self.maze[y][x] = blank_string
                print("Select the start position.")
            elif self.maze[clicked_coords[1]][clicked_coords[0]] == end_string:
                self.maze[clicked_coords[1]][clicked_coords[0]] = blank_string
                self.click_phase = 2
                print("Select the end point.")

    def clear(self, size, string = blank_string):
        self.maze = []
        for y in range(size[1]):
            row = []
            for x in range(size[0]):
                row.append(string)
            self.maze.append(row)

    def generate(self, mode, wait=False):
        self.solution = []
        
        self.clear([len(self.maze[0]),len(self.maze)], wall_string)

        start_cell = [-1,-1]
        while start_cell[0] % 2 == 1 and start_cell[0] < 0 and start_cell[1] % 2 == 1 and start_cell[1] < 0:
            start_cell = [randint(1,len(self.maze[0])-2),randint(1,len(self.maze)-2)]

        cells_list = []
        cells_list.append([start_cell[0],start_cell[1]])
        self.maze[cells_list[0][1]][cells_list[0][0]] = blank_string

        while len(cells_list) > 0:
            if mode == "prim":
                current_cell = choice(cells_list)
            elif mode == "snake":
                current_cell = cells_list[len(cells_list)-1]
            elif mode == "straight":
                current_cell = cells_list[0]
            else:
                print("Error: Invalid generation type '%s'" % mode)
                throw_error("Generation Type Invalid","Invalid generation type '%s'" % mode)

            if len(neighbors(self, current_cell)) > 0:
                random_neighbor = choice(neighbors(self, current_cell))
                self.maze[random_neighbor[1]][random_neighbor[0]] = blank_string

                if current_cell[0] > random_neighbor[0]:
                    self.maze[current_cell[1]][current_cell[0]-1] = blank_string
                if current_cell[0] < random_neighbor[0]:
                    self.maze[current_cell[1]][current_cell[0]+1] = blank_string
                if current_cell[1] > random_neighbor[1]:
                    self.maze[current_cell[1]-1][current_cell[0]] = blank_string
                if current_cell[1] < random_neighbor[1]:
                    self.maze[current_cell[1]+1][current_cell[0]] = blank_string

                cells_list.append(random_neighbor)

            else:
                cells_list.remove(current_cell)

            handle_events(self.screen)

            if wait:
                self.draw()

        end_x = randint(0,len(self.maze[0])-1)
        end_y = randint(0,len(self.maze)-1)

        start_x = randint(0,len(self.maze[0])-1)
        start_y = randint(0,len(self.maze)-1)

        while not (sqrt((start_x-end_x)**2 + (start_y-end_y)**2) > ((len(self.maze[0])+len(self.maze))/2)/2 * 1.5 and self.maze[end_y][end_x] == blank_string and self.maze[start_y][start_x] == blank_string):
            end_x = randint(0,len(self.maze[0])-1)
            end_y = randint(0,len(self.maze)-1)

            start_x = randint(0,len(self.maze[0])-1)
            start_y = randint(0,len(self.maze)-1)

        self.maze[end_y][end_x] = end_string
        self.maze[start_y][start_x] = start_string

        self.built = True

    def solve(self, wait=False, educational=False):
        global font
        if educational:
            font = pygame.font.SysFont("arial", int(self.tile_size/2))
            
        self.solution = []
        self.distances = []
        
        # Find the starting and ending cells and set initial distances
        for y in range(len(self.maze)):
            row = []
            for x in range(len(self.maze[y])):
                if self.maze[y][x] == start_string:
                    row.append(0)
                    start_coords = [x,y]
                elif self.maze[y][x] == end_string:
                    row.append(-1)
                    end_coords = [x,y]
                else:
                    row.append(-1)
            self.distances.append(row)

        mark_neighbors(self, self.distances[start_coords[1]][start_coords[0]], start_coords)

        while len(self.marking_stack) > 0:
            mark_neighbors(self, self.distances[self.marking_stack[0][1]][self.marking_stack[0][0]], self.marking_stack[0])
            try:
                self.marking_stack.pop(0)
            except IndexError:
                pass
            if educational:
                self.draw(educational)
                pygame.time.wait(100)
            handle_events(self.screen)

        current_coords = end_coords

        while self.distances[current_coords[1]][current_coords[0]] >= 1:
            current_coords = return_lower(self, current_coords)
            if self.distances[current_coords[1]][current_coords[0]] > 0:
                self.solution.append(current_coords)
            handle_events(self.screen)
            if wait or educational:
                self.draw(educational)
                if educational:
                    pygame.time.wait(10)

    def load(self, file_name=""):
        path = os.path.dirname(__file__)
        
        self.maze = []

        if file_name == "":
            file_name = input("What is the name of the file? (.txt files only, don't include the extension) ")

        file_name = os.path.join(path, "mazes", file_name + ".txt")

        file = open(file_name, "r")

        lines = file.readlines()

        lines.pop(0)

        for line in lines:
            line = line.replace("\n","")
            row = line.split(",")
            self.maze.append(row)

        file.close()

        self.built = True
